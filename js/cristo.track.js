$(function() {

    var seletores = [
        '#conteudo .bt-enviar-mensagem',
        '#conteudo .menu-superior a',
        '#conteudo .veja-video',
        'footer.main-footer .cif-content ul li a',
        'footer.main-footer .sesc-content a'
    ];
    var labels = [
        'destaque_envie-sua-mensagem',
        'menu-superior_{0}',
        'menu-superior_video',
        'rodape_{0}',
        'rodape_conheca-o-programa'
    ];

    for (var i = seletores.length - 1; i >= 0; i--) {
        var tagueado = $(seletores[i]);
        if( tagueado.length ){
            tagueado.data('trackSelector', seletores[i]);
            tagueado.data('trackLabelGeneric', labels[i]);

            tagueado.click(function(ev) {
                var label = $(this).data('trackLabelGeneric');
                var labelData = $(this).attr('data-trackLabel');

                ga('send', 'event', 'reveleabeleza', 'clique', label.format(labelData));
            });
        }
    };
});

