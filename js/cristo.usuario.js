var Usuario = function(cInstance) {
    this.cristo = cInstance;
    this.dados = null;
    this.$proximoPasso = $('#proximoPasso');
    this.$siteAuth = $('#siteAuth');
    this.$nome = $('#nome');
    this.$email = $('#email');

    this.$siteAuth.on('click.siteLogin', $.proxy(this.onSiteLogin, this));
}

// Get / Set
Usuario.prototype.setDados = function(o) { this.dados = o; };
Usuario.prototype.getDados = function() { return this.dados; };

// Chave para ligar/desligar o listener do login via site
Usuario.prototype.loginSiteSwitch = function(opt) {
    if(opt) {
        this.$proximoPasso.on( 'click.proximoPasso', $.proxy(this.onProximoPasso, this));
    } else {
        this.$proximoPasso.off( 'click.proximoPasso', $.proxy(this.onProximoPasso, this));
    }
};

// Valida formulário
Usuario.prototype.validaFormSite = function() {
    var emailTest = /^[\w-\._\+%]+@(?:[\w-]+\.)+[\w]{2,6}$/.test(this.$email.val()),
        nomeTest = (this.$nome.val().length > 2);

    if(nomeTest) this.$nome.removeClass("error-form");
    else this.$nome.addClass("error-form");

    if(emailTest) this.$email.removeClass("error-form");
    else this.$email.addClass("error-form");

    return (emailTest && nomeTest);
};

// Method do evento de clique do proximo passo com login via site
Usuario.prototype.onProximoPasso = function(ev) {
    ev = ev || event;
    ev.preventDefault();
    if(this.validaFormSite()){
        var dados = {
            "Nome": this.$nome.val(),
            "Email": this.$email.val(),
            "TipoUsuario": 1 // tipos - 1: Site | 2: Facebook | 3: Twitter
        }
        this.loginSiteSwitch(false);
        this.setDados(dados);
        window.location.hash = "#/enviar/3";
    } else {
        console.log("formulário inválido");
    }
}

// Method do evento de clique que ativa o login via site
Usuario.prototype.onSiteLogin = function(ev) {
    ev = ev || event;
    ev.preventDefault();
    this.loginSiteSwitch(true);
    this.cristo.Animacao.animarCamposNomeEmail();
}
//
