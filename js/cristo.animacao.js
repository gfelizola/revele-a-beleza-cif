var Animacao = function( cInstance ) {
    this.cristo            = cInstance;
    this.refFundo          = $('#main');
    this.enviarMensagem    = $("#enviarMensagem");
    this.etapas            = $("#enviarMensagem .etapa");
    this.etapaAtual        = 0;
    this.alturaTela        = 0;
    this.posFundoX         = 50;
    this.posFundoY         = 0;
    this.posBotao          = 0;
    this.animacaoEstrelas  = null;
    this.posDeviceX        = 0;
    this.qtdeEstrelasAtual = 0;
    this.isIE              = /msie/.test(navigator.userAgent.toLowerCase());
    this.isSafari          = (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1);
    this.isLive            = $('#main').hasClass('live');

    if( ! this.isIE ){
        this.isIE = !!navigator.userAgent.match(/Trident\//);
    }

    if( $.browser.mobile ){
        $('#player .player-flash').remove();
        $('#player .player-mobile').show();
    }

}

Animacao.prototype.animarEntrada = function() {
    var tl = new TimelineMax({delay:0.5, onCompleteScope:this, onComplete:function() {
        this.animarCristo();
        this.cristo.Navegacao.init();
    }});

    tl.from( $('#main').show(), 0.8, { css:{ opacity:0 }, ease:Quad.easeInOut });
    tl.from( $('#cristo .jesus'), 1.5, {css:{opacity:0}} );
    tl.from( $('footer.main-footer').show(), 0.5, {css:{opacity:0}, delay:-0.3});
    tl.from( $('#instrucoes').show(), 0.5, {css:{opacity:0}, delay:-0.3});

    this.acertarTamanhosParAnimacao();
    $(window).resize(this.acertarTamanhosParAnimacao);
};

Animacao.prototype.animarQtdeMensagens = function(qtde) {
    var ani = this;
    var fake = { n:this.qtdeEstrelasAtual };
    var tamanho = qtde.toString().length;
    TweenMax.to( fake, 5, { n:qtde, ease:Cubic.easeOut, onUpdate:function() {
        var num = Math.round(fake.n).toString();
        while(num.length < tamanho) num = "0" + num;
        $('#conteudo .numero-estrelas').html( '<p>' + num + ' <span class="txt-mensagens">' + dicionario[ani.cristo.idioma].qtdeEnviada + '</span></p>');
    }});
};
    
Animacao.prototype.animarEnvioEtapa1 = function() {
    this.fecharEtapaAtual(function() {
        this.etapaAtual = 1;
        this.animarEtapasHelper();

        var etapa = this.etapas.eq(0);
        etapa.removeClass('hidden');

        this.removerEstilosAtuais([
            etapa, 
            etapa.find('.boxes .box.titulo'), 
            etapa.find('.boxes .box.numero'), 
        ]);

        if( ! this.isIE ){
            TweenMax.from( this.etapas.eq(0).find('.boxes .box.numero'), 0.8, { css:{ marginLeft:-470 }, ease:Cubic.easeInOut});
            TweenMax.from( this.etapas.eq(0).find('.boxes .box.titulo'), 0.8, { css:{ width:0, paddingLeft:0, paddingRight:0 }, ease:Power2.easeOut, delay:0.8 });
            TweenMax.from( this.etapas.eq(0).find('.fechar'), 0.8, { css:{ opacity:0 }, ease:Power2.easeOut, delay:1 });
        }
    });

    // $('#enviarMensagem .etapa').hide();
    // $('#enviarMensagem .etapa-1').show();
};

Animacao.prototype.animarEnvioEtapa2 = function() {
    this.fecharEtapaAtual(function() {
        this.etapaAtual = 2;
        this.animarEtapasHelper();

        var etapa = this.etapas.eq(1);

        this.removerEstilosAtuais([
            etapa, 
            etapa.find('.boxes .box.titulo'), 
            etapa.find('.boxes .box.numero'), 
            etapa.find('.box.facebook'), 
            etapa.find('.box.email'),
            etapa.find('.fechar')
        ]);

        etapa.removeClass('hidden');

        if( ! this.isIE ){
            TweenMax.from( etapa.find('.boxes .box.numero'), 0.8, { css:{ marginLeft:-470 }, ease:Power2.easeInOut });
            TweenMax.from( etapa.find('.boxes .box.titulo'), 0.8, { css:{ width:0, paddingLeft:0, paddingRight:0 }, ease:Power2.easeOut, delay:0.8});
            TweenMax.from( etapa.find('.box.facebook'), 0.8, { css:{ top:-70 }, ease:Power2.easeOut, delay:1.2 });
            TweenMax.from( etapa.find('.box.email'), 0.8, { css:{ right:-70 }, ease:Power2.easeOut, delay:1.5 });
            TweenMax.from( etapa.find('.fechar'), 0.8, { css:{ opacity:0 }, ease:Power2.easeOut, delay:2 });
        }
        
    });
};
    
Animacao.prototype.animarCamposNomeEmail = function() {
    if( this.etapaAtual != 2 ){
        this.animarEnvioEtapa2();
    } else {
        var etapa = this.etapas.eq(1);

        this.removerEstilosAtuais([
            etapa, 
            etapa.find('.form-login'), 
            etapa.find('.form-login input'), 
            etapa.find('.proximo-passo'),
            etapa.find('.fechar')
        ]);

        etapa.find('.form-login').removeClass('hidden');
        etapa.find('.proximo-passo').removeClass('hidden');
        
        if( ! this.isIE ){
            TweenMax.from( etapa.find('.form-login'), 0.5, { css:{ height:0, paddingTop:0, paddingBottom:0 }, ease:Power2.easeOut, delay:0.5 });
            TweenMax.staggerFrom( etapa.find('.form-login input'), 0.5, { css:{ width:0, padding:0 }, ease:Power2.easeOut, delay:0.7 }, 0.2);
            TweenMax.from( etapa.find('.proximo-passo'), 0.5, { css:{ marginRight:-240 }, ease:Power2.easeOut, delay:1.4 });
        }
    }
};

Animacao.prototype.animarEnvioEtapa3 = function() {
    this.fecharEtapaAtual(function() {
        this.etapaAtual = 3;
        this.animarEtapasHelper();

        var etapa = this.etapas.eq(2);
        this.removerEstilosAtuais([
            etapa, 
            etapa.find('.boxes .box.titulo'), 
            etapa.find('.boxes .box.numero'), 
            etapa.find('.container-mensagem'), 
            etapa.find('.box.recaptcha'),
            etapa.find('.bt-enviar'),
            etapa.find('.fechar')
        ]);

        etapa.removeClass('hidden');

        if( ! this.isIE ){
            var tl = new TimelineMax();
            tl.from( etapa.find('.boxes .box.numero'), 0.8, { css:{ marginLeft:-470 }, ease:Power2.easeInOut });
            tl.from( etapa.find('.boxes .box.titulo'), 0.8, { css:{ width:0, paddingLeft:0, paddingRight:0 }, ease:Power2.easeOut});
            tl.from( etapa.find('.container-mensagem'), 0.5, { css:{ height:0, paddingBottom:0, paddingTop:0 }, ease:Power2.easeInOut });
            tl.from( etapa.find('.box.captcha'), 0.5, { css:{ height:0, paddingBottom:0, paddingTop:0 }, ease:Power2.easeInOut });
            tl.from( etapa.find('.bt-enviar'), 0.5, { css:{ marginRight:-160 }, ease:Power2.easeInOut });
            tl.from( etapa.find('.fechar'), 0.8, { css:{ opacity:0 }, ease:Power2.easeOut });
        }
    });
};
    
Animacao.prototype.animarEnvioEtapa4 = function() {
    this.fecharEtapaAtual(function() {
        this.etapaAtual = 4;
        this.animarEtapasHelper();

        var etapa = this.etapas.eq(3);
        this.removerEstilosAtuais([
            etapa, 
            etapa.find('.boxes .box.titulo'), 
            etapa.find('.boxes .box.numero'), 
            etapa.find('.box.retorno'),
            etapa.find('.fechar')
        ]);

        etapa.removeClass('hidden');

        var dados = this.cristo.Mensagem.mensagemAtual;
        etapa.find('.retorno .senha span').text(dados.Id);
        etapa.find('.titulo2 a').attr('href','#/visualizar/' + dados.Codigo);

        if( ! this.cristo.Mensagem.aovivo ){
            etapa.find('.retorno .senha').show();
            etapa.find('.retorno .aovivo').hide();
        } else {
            etapa.find('.retorno .senha').hide();
            etapa.find('.retorno .aovivo').show();
        }

        if( ! this.isIE ){
            var tl = new TimelineMax();
            tl.from( etapa.find('.boxes .box.numero'), 0.8, { css:{ marginLeft:-270 }, ease:Power2.easeOut });
            tl.from( etapa.find('.boxes .box.titulo'), 0.8, { css:{ marginLeft:-570 }, ease:Power2.easeOut });
            tl.from( etapa.find('.box.retorno'), 0.5, { css:{ height:0, paddingBottom:0, paddingTop:0 }, ease:Power2.easeOut });
            tl.from( etapa.find('.fechar'), 0.8, { css:{ opacity:0 }, ease:Power2.easeOut });
        }
    });
};

Animacao.prototype.animarFeedbackErro = function(texto) {
    var boxFeedback = this.etapas.eq(2).find('.feedback');

    this.removerEstilosAtuais([boxFeedback]);
    boxFeedback.removeClass('hidden').text( texto );

    if( ! this.isIE ){
        TweenMax.from( boxFeedback, 0.7, { css:{ height:0 }, ease:Back.easeOut });
    }
};

Animacao.prototype.animarEtapasHelper = function(etapa) {
    if( this.enviarMensagem.hasClass('hidden') ){
        this.enviarMensagem.show().removeClass('hidden');
    };
};

Animacao.prototype.fecharEtapaAtual = function(callback) {
    if( this.etapaAtual == 0 ){
        callback.apply(this);
    } else {
        var tl = new TimelineMax({onCompleteScope:this, onComplete:function() {
            this.etapas.addClass('hidden');
            callback.apply(this);
        }});

        if( this.etapaAtual == 1 ){
            tl.to( this.etapas.eq(0).find('.fechar'), 0.5, { css:{ opacity:0 }, ease:Power2.easeInOut });
            tl.to( this.etapas.eq(0).find('.boxes .box.numero'), 0.5, { css:{ marginLeft:-470 }, ease:Power2.easeInOut });
            tl.to( this.etapas.eq(0).find('.boxes .box.titulo'), 0.5, { css:{ width:0, paddingLeft:0, paddingRight:0 }, ease:Power2.easeInOut, onCompleteScope:this, onComplete:function() {
                this.etapas.eq(0).addClass('hidden');
                callback.apply(this);
            }});
        } else if( this.etapaAtual == 2 ){

            var etapa = this.etapas.eq(1);
            if( ! etapa.find('.form-login').hasClass('hidden') ){
                tl.to( etapa.find('.proximo-passo'), 0.5, { css:{ marginRight:-240 }, ease:Power2.easeOut });
                tl.to( etapa.find('.form-login'), 0.5, { css:{ height:0, paddingTop:0, paddingBottom:0 } } )
            }
            
            tl.to( etapa.find('.fechar'), 0.5, { css:{ opacity:0 }, ease:Power2.easeInOut });
            tl.to( etapa.find('.box.email'), 0.8, { css:{ right:-70 }, ease:Power2.easeOut, delay:-0.5 });
            tl.to( etapa.find('.box.facebook'), 0.8, { css:{ top:-70 }, ease:Power2.easeOut, delay:-0.5 });
            tl.to( etapa.find('.boxes .box.numero'), 0.8, { css:{ marginLeft:-470 }, ease:Power2.easeInOut, delay:-0.5 });
            tl.to( etapa.find('.boxes .box.titulo'), 0.8, { css:{ width:0, paddingLeft:0, paddingRight:0 }, ease:Power2.easeOut, delay:-0.3});

        } else if( this.etapaAtual == 3 ){

            var etapa = this.etapas.eq(2);
            tl.to( etapa.find('.fechar'), 0.5, { css:{ opacity:0 }, ease:Power2.easeInOut });
            tl.to( etapa.find('.bt-enviar'), 0.5, { css:{ marginRight:-160 }, ease:Power2.easeInOut });
            tl.to( etapa.find('.box.feedback'), 0.5, { css:{ height:0, paddingBottom:0, paddingTop:0 }, ease:Power2.easeInOut, delay:-0.5 });
            tl.to( etapa.find('.box.captcha'), 0.5, { css:{ height:0, paddingBottom:0, paddingTop:0 }, ease:Power2.easeInOut, delay:-0.3 });
            tl.to( etapa.find('.container-mensagem'), 0.5, { css:{ height:0, paddingBottom:0, paddingTop:0 }, ease:Power2.easeInOut, delay:-0.3 });
            tl.to( etapa.find('.boxes .box.numero'), 0.8, { css:{ marginLeft:-470 }, ease:Power2.easeInOut, delay:-0.3 });
            tl.to( etapa.find('.boxes .box.titulo'), 0.8, { css:{ width:0, paddingLeft:0, paddingRight:0 }, ease:Power2.easeOut, delay:-0.5 });

        } else if( this.etapaAtual == 4 ){
            var etapa = this.etapas.eq(3);
            tl.to( etapa.find('.fechar'), 0.5, { css:{ opacity:0 }, ease:Power2.easeInOut });
            tl.to( etapa.find('.box.retorno'), 0.5, { css:{ height:0, paddingBottom:0, paddingTop:0 }, ease:Power2.easeOut });
            tl.to( etapa.find('.boxes .box.titulo'), 0.8, { css:{ marginLeft:-270 }, ease:Power2.easeOut});
            tl.to( etapa.find('.boxes .box.numero'), 0.8, { css:{ marginLeft:-270 }, ease:Power2.easeOut });
        }
    }
};

Animacao.prototype.animarCristo = function () {
    var ani = this;
    if( ! this.isSafari && ! this.isLive ){
        $(window).bind('mousemove.fundo',function(ev) {
            ani.setarPosicoesAnimadas(ev.pageX);
        });
    }

    if (window.DeviceMotionEvent) {
        window.addEventListener('devicemotion', function(ev) {
            var posicao = Math.round(ev.accelerationIncludingGravity.x);
            if( posicao != ani.posDeviceX ){
                ani.posDeviceX = posicao;
                ani.setarPosicoesAnimadas(mapNumber( posicao, -5, 5, 0, $(window).width() ));
            }
        }, false);
    }

    if( this.alturaTela > 0 ){
        $(window).bind('scroll.fundo', $.proxy(function(ev) {
            var st = $(window).scrollTop();
            this.posFundoY = mapNumber(st, 0, this.alturaTela, 0, 40 );
            if( this.posFundoY > 100 ) this.posFundoY = 100;
            $('#main').css({ backgroundPosition: this.posFundoX + '% ' + this.posFundoY + '%' });
            
            if( ! $.browser.mobile ) this.verificarMenuTopo(st);
        }, this));
        $(window).trigger('scroll.fundo');
    }
};

Animacao.prototype.setarPosicoesAnimadas = function(posX) {
    var ww         = this.refFundo.width();
    var inicio     = ww * 0.3;
    var fim        = ww - inicio;

    if( posX > inicio && posX < fim ){

        this.posFundoX = mapNumber( posX, 0, ww, 0, 110 );

        var diff = mapNumber( 1, 0, 100, 0, ww );

        var angulo = mapNumber( posX, inicio, fim, -3, 3 );
        TweenMax.killTweensOf( $("#cristo .jesus") );
        TweenMax.killTweensOf( $("#cristo .estrelas") );
        TweenMax.killTweensOf( $("#main") );

        // TweenMax.to( $("#cristo .jesus"), 0.5, { left: (angulo * -1) + diff, transformPerspective:1500, rotationY: angulo });
        TweenMax.to( $("#cristo .jesus"), 0.5, { transformPerspective:1500, rotationY: angulo });
        // TweenMax.to( $("#cristo .estrelas"), 0.5, { left: (angulo * -1) + diff });
        TweenMax.to( $('#main'), 0.5, { css:{ 'background-position': this.posFundoX + '% ' + this.posFundoY + '%' }});
    }
};

Animacao.prototype.piscarEstrelas  = function() {
    // TweenMax.set( $('#cristo .estrelas .estrelas-item .estrelas-item-icon'), {css:{ opacity:0.5 }});
    TweenMax.staggerFromTo( $('#cristo .estrelas .estrelas-item .estrelas-item-icon'), 0.5, { css:{ opacity:1 }}, { css:{ opacity:0.5 }, ease:Linear.easeNone, repeat:-1, yoyo:true }, 0.2);
};

Animacao.prototype.pararEstrelas  = function() {
    if(this.animacaoEstrelas) this.animacaoEstrelas.pause();
};

Animacao.prototype.apagarEstrelas  = function() {
    TweenMax.to( $('#cristo .estrelas .estrelas-item .estrelas-item-icon'), 0.5, { css:{ opacity:0.1 }, ease:Linear.easeNone });
};

Animacao.prototype.acenderEstrelas  = function() {
    var ani = this;
    TweenMax.staggerFrom( $('#cristo .estrelas .estrelas-item .estrelas-item-icon').show(), 0.5, { css:{ opacity:0 }, ease:Linear.easeNone }, 0.05, function() {
        ani.piscarEstrelas();
    });
};

Animacao.prototype.verificarMenuTopo = function(st) {
    // if( st > this.posBotao ){
    //     if( ! $('#container').hasClass('fixo') ){
    //         $('#container').addClass('fixo');
    //         $('#main').css({paddingTop:this.posBotao});
    //         this.animarMenuTopo(0,-70);
    //     }
    // } else {
    //     if( $('#container').hasClass('fixo') && ! TweenMax.isTweening( $('#conteudo') ) ) 
    //         this.animarMenuTopo(-70,0);
    // }
};

Animacao.prototype.animarMenuTopo = function(topIn, topOut) {
    var vars = { css:{ top:topIn }, ease:Power2.easeInOut };
    if( topIn < 0 ){
        vars.onComplete = function() {
            $('#container').removeClass('fixo');
            $('#main').css({paddingTop:0});

            TweenMax.fromTo( $('#conteudo'), 0.5, {css:{opacity:0}}, {css:{opacity:1, top:0}});
        }
    }
    TweenMax.killTweensOf($('#conteudo'));
    TweenMax.fromTo( $('#conteudo'), 0.5, {css:{top:topOut}}, vars);
};

Animacao.prototype.pararAnimacaoDoCristo = function() {
    $(window).unbind('mousemove.fundo');

    this.removerEstilosAtuais([
        $("#cristo .jesus"),
        $("#cristo .estrelas")
    ]);
};

Animacao.prototype.acertarTamanhosParAnimacao = function(ev) {
    this.alturaTela = $(document).height() - $(window).height();// - $('footer').height();
    this.posBotao = $('#conteudo').height() - 40;
    $(window).trigger('scroll.fundo');

    var sw, sh,
        ow = 1600,
        oh = 1900,
        ratio = 1.1,
        ww = $("#main").width(),
        wh = $("#main").height(),
        scala = mapNumber( ww, 0, ow, 0, 100 );


    sw = ww * ratio;
    sh = mapNumber( scala, 0, 100, 0, oh ) * ratio;

    // if( ww > ow ){
    //     sw = ww * ratio;
    //     sh = wh * ratio;
    // } else {
    //     if( ww * ratio < ow ){
    //         sw = ow;
    //         sh = oh;
    //     } else {
    //         sw = ow * ratio;
    //         sh = oh * ratio;
    //     }
    // }

    $("#main").css({ 'background-size': sw + 'px ' + sh + 'px'});
    // console.log( ww, wh );
    // $("#main").css({ 'background-size': ww + 'px ' + wh + 'px'});
};

Animacao.prototype.removerEstilosAtuais = function(elementos) {
    if( $.isArray(elementos) ){
        $.each(elementos, function(index, val) {
            $(val).attr('style', '');
        });
    } else {
        $(elementos).attr('style', '');
    }
};
//
