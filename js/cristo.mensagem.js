var Mensagem = function(cInstance) {
    this.cristo = cInstance;
    this.$mensagem = $('#mensagem');
    this.$btEnviarMensagem = $('#btEnviarMensagem');
    this.$btAtualizarCaptcha = $('#enviarMensagem .etapa-3 .atualizar-captcha');
    this.$feedbackErro = $('#feedbackErro');
    this.mensagemEnviada = false;
    this.mensagemAtual = {};
    this.dados = null;
    this.aovivo = false;

    // // Init Countdown
    this.$mensagem.countChars({
        "selectorCounter": "> span.contador:eq(0)",
        "total": 140
    });
};

// Get / Set
Mensagem.prototype.setDados = function(o) { this.dados = o; };
Mensagem.prototype.getDados = function() { return this.dados; };

// Chave para ligar/desligar o listener de enviar mensagem
Mensagem.prototype.enviarMsgSwitch = function(opt) {
    if(opt) {
        this.$btEnviarMensagem.on( 'click.enviarMsg', $.proxy(this.onEnviaMsg, this));
        this.$btAtualizarCaptcha.on( 'click.captchaUpdate', $.proxy(this.atualizarCaptcha, this));
    } else {
        this.$btEnviarMensagem.off( 'click.enviarMsg', $.proxy(this.onEnviaMsg, this));
        this.$btAtualizarCaptcha.on( 'click.captchaUpdate', $.proxy(this.atualizarCaptcha, this));
    }
};

// Valida formulário
Mensagem.prototype.validaMsg = function() {
    var txtMsg = this.$mensagem.val();
    if(txtMsg.length > 0 && txtMsg.length <= 140) this.$mensagem.removeClass("error-form");
    else this.$mensagem.addClass("error-form");

    var captcha = $('#CaptchaInputText');
    var txtCap = captcha.val();
    if(txtCap.length > 0 ) captcha.removeClass("error-form");
    else captcha.addClass("error-form");

    return ( $('#enviarMensagem .error-form').length <= 0 );
};

Mensagem.prototype.atualizarCaptcha = function(ev) {
    ev = ev || event;
    if(ev) ev.preventDefault();

    this.cristo.Servico.CarregarCaptcha();
}

Mensagem.prototype.resetar = function() {
    this.cristo.Drag.draggieStarSetPosition(null);
    this.cristo.Usuario.$nome.val('');
    this.cristo.Usuario.$email.val('');
    this.cristo.Usuario.setDados(null);
    this.$mensagem.val('');
    $('#CaptchaInputText').val('');
    $('#CaptchaDeText').val('');
    this.setDados(null);
};

// Method do evento de clique do proximo passo com login via site
Mensagem.prototype.onEnviaMsg = function(ev) {
    ev = ev || event;
    ev.preventDefault();

    var msg = this;

    if(this.validaMsg()){
        this.dados = {
            "Texto": this.$mensagem.val(),
            "CaptchaInputText": $('#CaptchaInputText').val(),
            "CaptchaDeText": $('#CaptchaDeText').val()
        };
        this.enviarMsgSwitch(false);
        this.cristo.Servico.EnviarMensagem(function(retorno) {
            msg.enviarMsgSwitch(true);

            if(parseInt(retorno.status, 10) === 0) {
                msg.mensagemEnviada = true;
                msg.mensagemAtual = retorno.dados;
                msg.aovivo = retorno.aovivo == 1;
                window.location.hash = "#/enviar/4";
            } else {
                msg.cristo.Animacao.animarFeedbackErro(retorno.feedback);
                msg.cristo.Servico.CarregarCaptcha(function() {
                    if( retorno.status == 3 ){
                        if( retorno.feedback.toLowerCase().indexOf('captcha') >= 0 ){
                            $('#CaptchaInputText').addClass('error-form');
                        }
                    } else if( retorno.status == 2 ){
                        TweenMax.delayedCall(3, function() {
                            msg.resetar();
                            msg.cristo.Animacao.fecharEtapaAtual(function(){
                                msg.cristo.Drag.$draStar.addClass('hidden');
                                msg.cristo.Animacao.etapaAtual = 0;
                            });
                            window.location.hash = "inicio";
                        })
                    }
                });

            }
        });
    } else {
        console.log("formulário inválido");
    }
};
//

