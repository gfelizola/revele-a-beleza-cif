var Estrelas = function(cInstance){
    this.cristo = cInstance;
    this.qtdeAtual = 0;
    this.qtdeTotal = 250;
    this.ultimoCodigo = '';
    this.chamarFila = true;
    this.chamadaFila = null;
    this.montarGrid();
    this.montarGridProjecao();
};

Estrelas.prototype.montarGrid = function() {
    var tamanho   = 2;
    var linhas    = 100/tamanho;
    var colunas   = 100/tamanho;
    var container = $('#cristo .mapa-cristo');
    var mapaUteis = [
        //[ primiro Y, primeiro X, qtde seguintes X ]
        [ 0,  24, 2  ],
        [ 1,  23, 3  ],
        [ 2,  23, 3  ],
        [ 3,  23, 3  ],
        [ 4,  23, 4  ],
        [ 5,  23, 4  ],
        [ 6,  22, 5  ],
        [ 7,  23, 4  ],
        [ 8,  20, 8  ],
        [ 9,  3,  2  ],
        [ 9,  15, 17 ],
        [ 9,  43, 4  ],
        [ 10, 1,  45 ],
        [ 11, 4,  39 ],
        [ 12, 7,  34 ],
        [ 13, 8,  32 ],
        [ 14, 8,  31 ],
        [ 15, 9,  30 ],
        [ 16, 10, 27 ],
        [ 17, 17, 15 ],
        [ 18, 18, 12 ],
        [ 19, 18, 12 ],
        [ 20, 18, 12 ],
        [ 21, 18, 12 ],
        [ 22, 18, 12 ],
        [ 23, 18, 13 ],
        [ 24, 18, 13 ],
        [ 25, 18, 13 ],
        [ 26, 18, 13 ],
        [ 27, 18, 13 ],
        [ 28, 19, 12 ],
        [ 29, 19, 12 ],
        [ 30, 19, 12 ],
        [ 31, 19, 12 ],
        [ 32, 19, 12 ],
        [ 33, 19, 12 ],
        [ 34, 19, 12 ],
        [ 35, 19, 12 ],
        [ 36, 20, 11 ],
        [ 37, 20, 11 ],
        [ 38, 20, 11 ],
        [ 39, 20, 11 ],
        [ 40, 20, 11 ],
        [ 41, 20, 11 ],
        [ 42, 20, 11 ],
        [ 43, 20, 11 ],
        [ 44, 20, 10 ],
        [ 45, 20, 10 ],
        [ 46, 20, 10 ],
        [ 47, 20, 10 ],
        [ 48, 20, 10 ],
        [ 49, 19, 11 ]
    ];

    for (var i = linhas - 1; i >= 0; i--) {
        for (var j = colunas - 1; j >= 0; j--) {
            var area = $('<div></div>').addClass('area');

            var isOk = false;
            for (var k = mapaUteis.length - 1; k >= 0; k--) {
                var y = mapaUteis[k][0];
                var xi = mapaUteis[k][1];
                var xf = mapaUteis[k][2];

                if (i == y) {
                    if (j >= xi && j < xi + xf) isOk = true;
                }
            };

            var porcX = (i * tamanho);
            var porcY = (j * tamanho);

            area
                .css({
                    width: tamanho + '%',
                    height: tamanho + '%',
                    top: porcX + '%',
                    left: porcY + '%'
                })
                .data('pos-x', j)
                .data('pos-y', i)
                .addClass('posicao-' + j + '-' + i);

            if (isOk){
                area.addClass('area-ok');
            }

            container.append(area);
        }
    }
};

Estrelas.prototype.montarGridProjecao = function() {
    var tamanho = 10;
    var linhas = Math.ceil(100 / tamanho);
    var colunas = Math.ceil(100 / tamanho);
    var container = $('#cristo .mapa-cristo-projecao');
    var mapaUteis = [
        [0,  4, 2],
        [1,  4, 2],
        [2,  0, 10],
        [3,  2, 6],
        [4,  4, 2],
        [5,  4, 2],
        [6,  4, 2],
        [7,  4, 2],
        [8,  4, 2],
        [9,  4, 2]
    ];

    for (var i = linhas - 1; i >= 0; i--) {
        for (var j = colunas - 1; j >= 0; j--) {
            var area = $('<div></div>').addClass('area');

            var isOk = false;
            for (var k = mapaUteis.length - 1; k >= 0; k--) {
                var y = mapaUteis[k][0];
                var xi = mapaUteis[k][1];
                var xf = mapaUteis[k][2];

                if (i == y) {
                    if (j >= xi && j < xi + xf) isOk = true;
                }
            };

            var porcX = (i * tamanho);
            var porcY = (j * tamanho);

            area
                .css({
                    width: tamanho + '%',
                    height: tamanho + '%',
                    top: porcX + '%',
                    left: porcY + '%'
                })
                .data('pos-x', j)
                .data('pos-y', i)
                .addClass('posicao-' + j + '-' + i);

            if (isOk){
                area.addClass('area-ok');
            }

            container.append(area);
        }
    }
};

Estrelas.prototype.popular = function(lista, aberta, limpar) {
    console.log( 'Estrelas.popular', lista.length, this.qtdeAtual, limpar );

    var tpl = '<div class="estrelas-item" style="top: {0}%; left: {1}%;" data-ref="{7}">';
        tpl += '<i class="estrelas-item-icon">';
        tpl += '<img src="img/estrela-cristo-{7}.png" alt="" />'
        tpl += '</i>';
        tpl += '<i class="estrelas-item-line"></i>';
        tpl += '<div class="estrelas-item-wrap">';
        tpl += '<div class="estrelas-item-content">';
        tpl +=      '<span class="estrelas-item-name">{2}</span>';
        tpl +=      '<hr>';
        tpl +=      '<p class="estrelas-item-text">{3}</p>';
        tpl += '</div>';
        tpl += '<div class="estrelas-item-image">';
        tpl +=      '<img src="{4}" height="106" width="106" alt="">';
        tpl += '</div>';
        tpl += '<a href="{5}" target="_blank" class="share-link share-twitter"><i></i></a>';
        tpl += '<a href="{6}" target="_blank" class="share-link share-facebook"><i></i></a>';
        tpl += '</div>';
        tpl += '</div>';

    var FBUrl = "http://www.facebook.com/sharer.php?s=100&p[title]={0}&p[url]={1}&p[summary]={2}&p[images][0]={3}";
    var container = $('#cristo .estrelas');

    if( limpar ){
        this.qtdeAtual = 0;
        this.closeAnyActive();
        container.empty();
    }

    var c = this.qtdeAtual;
    var rr = 0;
    for (var i = 0; i < lista.length; i++) {
        var item       = lista[i];
        var tipo       = Math.round( Math.random() * 2 ) + 1;
        var urlTratada = encodeURIComponent(item.Permalink);
        var linkFB = FBUrl.format(
            'Revele a Beleza - CIF', 
            urlTratada,
            'Cif e você vão fazer o cristo redentor brilhar. E você vai assistir ao vivo.',
            '');

        var strEstrela = tpl.format(
            item.Porcentagem_Y,
            item.Porcentagem_X,
            item.Nome,
            item.Texto,
            item.FotoUrl || "",
            'https://twitter.com/intent/tweet?url=' + urlTratada,
            item.Permalink,
            tipo
        );

        var posX = Math.round(item.Porcentagem_X);
        var posY = Math.round(item.Porcentagem_Y);

        var classPosicao = 'posicao-' + posX + '-' + posY;
        var estrela = $(strEstrela);
        estrela.addClass(classPosicao);
        estrela.find('.estrelas-item-icon').addClass('estrela-' + tipo).hide();
        if( item.Porcentagem_X > 50 ) estrela.addClass('estrelas-item-left');
        if( item.Porcentagem_Y > 90 ) estrela.addClass('estrelas-item-bottom');
        if( ! item.FotoUrl ) estrela.addClass('without-image');

        var matrizAtual = [
            [{x:posX-1, y:posY-1},  {x:posX, y:posY-1}, {x:posX+1, y:posY-1}],
            [{x:posX-1, y:posY},    {x:posX, y:posY},   {x:posX+1, y:posY}],
            [{x:posX-1, y:posY+1},  {x:posX, y:posY+1}, {x:posX+1, y:posY+1}]
        ];
        var temNaPosicao = false;

        for (var xx = 0; xx < matrizAtual.length; xx++) {
            var linha = matrizAtual[xx];
            for (var yy = 0; yy < linha.length; yy++) {
                var coluna = linha[yy];

                if( $('#cristo .estrelas-item.posicao-' + coluna.x + '-' + coluna.y).length ) temNaPosicao = true;
            };
        };

        if( ! temNaPosicao ){
            c++;
            container.append(estrela);
        } else {
            rr++;
        }
        if( aberta ){
            if( item.Codigo == aberta ){ this.open(estrela); }
        }

        
        if( c >= this.qtdeTotal ) { break; }
    };

    this.qtdeAtual = c;
    if( aberta != null ) this.chamarFila = false;

    if( this.qtdeAtual >= this.qtdeTotal ){
        console.log( this.qtdeAtual, c, rr );

        this.configurarEstrelas( this.chamarFila  );
        this.cristo.Animacao.acenderEstrelas();
    } else {
        if( item.Codigo != this.ultimoCodigo ){
            this.ultimoCodigo = item.Codigo;
            this.cristo.Servico.ListarEstrelas(function(retorno) {
                this.cristo.Estrelas.popular(retorno.Mensagens);
            }, item.Codigo);
        } else {
            this.configurarEstrelas( this.chamarFila  );
            this.cristo.Animacao.acenderEstrelas();
        }
    }
};

Estrelas.prototype.configurarEstrelas = function(chamarFila){
    var root = this;
    $('#cristo').click(function(ev) {
        var off  = $(this).offset();
        var off2 = $(this).find('.estrelas').position();
        var posX = Math.round( ev.pageX - off.left - off2.left );
        var posY = Math.round( ev.pageY - off.top );
        var cw   = $(this).width();
        var ch   = $(this).height();

        if( posX > 0 && posX < cw && posY > 0 && posY < ch ){
            var porcX  = Math.floor( mapNumber( posX, 0, cw, 0, 100 )),
                porcY  = Math.floor( mapNumber( posY, 0, ch, 0, 100 )),
                toOpen = [];

            if ( $('.estrelas-item.posicao-' + porcX + '-' + porcY).length ){
                toOpen.push( $('.estrelas-item.posicao-' + porcX + '-' + porcY) );
            } else {
                var margem = 2,
                    maxX   = porcX + margem,
                    minX   = porcX - margem,
                    maxY   = porcY + margem,
                    minY   = porcY - margem;

                for (var i = minX; i <= maxX; i++) {
                    for (var j = minY; j <= maxY; j++) {
                        if ( $('.estrelas-item.posicao-' + i + '-' + j).length ){
                            toOpen.push( $('.estrelas-item.posicao-' + i + '-' + j) );
                            break;
                        }
                    };
                };
            }

            if( toOpen.length ){
                var el = toOpen[0];
                root.closeAnyActive(el);
                root.open(el);
            } else {
                root.closeAnyActive();
            }
        }
    });

    if( ! $.browser.mobile ){
        $('.estrelas-item .share-facebook').click(function(ev){
            ev = ev || event;
            if(ev) ev.preventDefault();

            var link = $(this).attr('href');
            var texto = $(this).parents('.estrelas-item').find('.estrelas-item-text').text();

            if( FB ){
                FB.ui({
                    method: 'feed',
                    display: 'popup',
                    name: 'Revele a Beleza - CIF',
                    link: link,
                    picture: 'http://www.reveleabeleza.com.br/img/site-image.jpg',
                    caption: 'Cif e você vão fazer o Cristo Redentor brilhar. E você vai assistir ao vivo.',
                    description: texto
                },
                function(response) {
                    if (response && response.post_id) {
                    // alert('Post was published.');
                    } else {
                    // alert('Post was not published.');
                    }
                });
            }
        });

        $('.estrelas-item .share-twitter').click(function(ev){
            ev = ev || event;
            if(ev) ev.preventDefault();

            var link = $(this).attr('href');
            window.open(link, 'Twitter', 'width=600,height=250,scrollbar=1,location=0,menubar=0,status=0,toolbar=0,titlebar=0');
        });
    }
};

Estrelas.prototype.iniciarFila = function() {
    if( this.chamarFila ){
        this.chamadaFila = TweenMax.delayedCall( 60, this.carregarNovaFila, null, this );
    }
};

Estrelas.prototype.carregarNovaFila = function() {
    TweenMax.staggerTo( $('.estrelas-item'), 0.5, {css:{opacity:0}}, 0.01, this.cristo.Navegacao.mostrarEstrelas, null, this );
};

// Fecha estrelas com base na classe .estrelas-item
Estrelas.prototype.close = function($this) {
    var root = this;
    root.animating = true;

    if (Modernizr.cssanimations) {
        $this.find('.estrelas-item-wrap').removeClass('flipInX').addClass('flipOutX');
    } else {
        $this.find('.estrelas-item-wrap').hide();
    }

    setTimeout(function() {
        $this.find('.estrelas-item-line').stop().animate({
            width: 0,
            opacity: 0
        }, 400, function() {
            $this.removeClass('active');
            root.animating = false;
        });
    }, 300);
};

// Abre estrelas com base na classe .estrelas-item
Estrelas.prototype.open = function($this) {
    var root = this;

    root.animating = true;
    $this.addClass('active');

    $this.find('.estrelas-item-line')
    .css({
        display: 'block',
        width: 0,
        opacity: 0
    })
    .stop()
    .animate({
        width: 75,
        opacity: 1
    }, 400, function() {
        if (Modernizr.cssanimations) {
            $this.find('.estrelas-item-wrap').show().removeClass('flipOutX').addClass('animated flipInX');
        } else {
            $this.find('.estrelas-item-wrap').fadeIn();
        }
        root.animating = false;
    });
};

Estrelas.prototype.closeAnyActive = function ($lessThis) {
    // var $this =$('.estrelas-item.active').not($lessThis || undefined);
    $('#cristo .estrelas .estrelas-item').not($lessThis || undefined).find('.estrelas-item-wrap').hide();
    $('#cristo .estrelas .estrelas-item').not($lessThis || undefined).find('.estrelas-item-line').hide();

    // if ($this.length !== 0) {
    //     var est = this;
    //     $this.each(function(index, el) {
    //         est.close($this);
    //     });
        
    // }
};