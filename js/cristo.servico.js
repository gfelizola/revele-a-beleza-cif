var Servico = function(cInstance) {
    this.cristo = cInstance;
    this.pathServico = urlPath().replace('front','') + '/Mensagem/';
    this.fila = [];
    this.estaChamando = false;
    this.padrao = {
        "async": true,
        "data": null,
        "dataType": "json",
        "type": "POST",
        "cache": false,
        "crossDomain": false,
        "xhrFields": {
            "withCredentials": false
        }
    };

    var wlh = window.location.href.toLowerCase();
    var locais = ['localhost','127.0.0.1','fenix']
    for (var i = 0; i < locais.length; i++) {
        if( wlh.indexOf(locais[i]) != -1 ) this.pathServico = 'http://fenix:81/Unilever/Cif/Mensagem/';
    };

    console.log( "pathServico", this.pathServico );
}

/*===== FILA DE CHAMADAS =====*/
Servico.prototype.adicionarChamada = function(metodo, parametros, callback) {
    this.fila.push( {
        metodo: metodo,
        params: parametros,
        callback: callback
    });

    if( ! this.estaChamando ) this.chamarFila();
};

Servico.prototype.chamarFila = function(){
    if( this.fila.length > 0 ){
        this.realizaChamada();
    } else {
        this.estaChamando = false;
    }
};

Servico.prototype.realizaChamada = function() {
    var chamada = this.fila.shift(),
        that = this,
        url = this.pathServico + chamada.metodo;

    NProgress.start();
    var jqxhr = $.ajax(url, chamada.params)
        .done(function(data, textStatus, jqXHR) {
            if( chamada.callback != null ){
                chamada.callback.apply(that, [data]);
                that.chamarFila();
            }
        })
        .fail(function(jqXHR, textStatus, errorThrown) {
            console.log( "Service error", textStatus, errorThrown);
        })
        .always(function() {
            NProgress.done();
        });
};


/*===== METODOS BACKEND =====*/
Servico.prototype.EnviarMensagem = function(callback) {
    var dadosMensagem = $.extend({},
        this.cristo.Drag.draggieStarGetPosition(),
        this.cristo.Usuario.getDados(),
        this.cristo.Mensagem.getDados(),
        { Idioma: (this.cristo.idioma == 'en' ? 2 : 1) }
    );

    console.log( 'dadosMensagem', dadosMensagem );

    var dadosEnvio = $.extend(this.padrao, {
        "data": dadosMensagem,
        "type": "POST"
    });
    
    this.adicionarChamada('Enviar', dadosEnvio, function(retorno) {
        if( callback ){
            callback.apply(this, [retorno]);
        }
    });
};

Servico.prototype.ListarEstrelas = function(callback, codigo) {
    var dadosEnvio = $.extend(this.padrao, {
        "type": "GET",
    });

    var codigo = codigo || "" ;
    this.adicionarChamada('Listar/' + codigo, dadosEnvio, function(retorno) {
        if( callback ){
            callback.apply(this, [retorno]);
        }
    });
};

Servico.prototype.CarregarCaptcha = function(callback) {
    NProgress.start();
    var servico = this;
    $('#enviarMensagem .captcha .container-captcha').load(this.pathServico + 'RetornaCaptcha?idioma=' + this.cristo.idioma, function(){
        NProgress.done();
        if( callback ) callback.apply(servico);
    });
};
//
