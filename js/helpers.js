// Console
if (!(window.console && console.log)) {
    (function() {
        var noop = function() {};
        var methods = ["assert", "clear", "count", "debug", "dir", "dirxml", "error", "exception", "group", "groupCollapsed", "groupEnd", "info", "log", "markTimeline", "profile", "profileEnd", "markTimeline", "table", "time", "timeEnd", "timeStamp", "trace", "warn"];
        var length = methods.length;
        var console = window.console = {};
        while (length--) {
            console[methods[length]] = noop;
        }
    }());
}

// Trim
if (!String.prototype.trim) {
    String.prototype.trim = function() {
        return this.replace(/^\s+|\s+$/g, '');
    };
}

// Format
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined' ? args[number] : match ;
        });
    };
}

// Urlify
if (!String.prototype.urlify) {
    String.prototype.urlify = function() {
        return this.replace(/\s(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig, function(url) {
            return '<a href="' + url + '" target="_blank">' + url + '</a>';
        });
    };
}

// Emailfy
if (!String.prototype.emailfy) {
    String.prototype.emailfy = function() {
        return this.replace(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi, function(email) {
            return '<a href="mailto:' + email + '">' + email + '</a>';
        });
    };
}

// Load Css
function loadCss(url) {
    var link = document.createElement("link");
    link.type = "text/css";
    link.rel = "stylesheet";
    link.href = url;
    document.getElementsByTagName("head")[0].appendChild(link);
}

// Load Script
function loadScript(url) {
    var script = document.createElement("script");
    script.type = "text/javascript";
    script.src = url;
    document.getElementsByTagName("head")[0].appendChild(script);
}

// Find object
function getObjects(obj, key, val, only) {
    only = only || false;
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val, only));
        } else if (i == key && obj[key] == val) {
            if(only) {
                if(!!obj[only])
                    objects.push(obj);
            } else
                objects.push(obj);
        }
    }
    return objects;
}

// Retorna o o diretorio atual ex.: http://xxx.com/dir/awesome/index.html -> /dir/awesome
function urlPath() {
    var path = String(window.location.pathname).split('/');
    path.pop();
    return path.join('/');
}



// regra de 3
function mapNumber(value, min1, max1, min2, max2) {
    return min2 + (max2 - min2) * ((value - min1) / (max1 - min1));
}