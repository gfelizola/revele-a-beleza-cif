var Drag = function(cInstance){
    this.cristo = cInstance;
    this.$mapaCristo = $('#mapaCristo');
    this.$draStar = $('#dragStar');
    this.draggieStarPosition = null;
    this.draggieStar = null;
    
}

// Inicializa o drag da estrela
Drag.prototype.draggieStarInitialize = function() {
    if (this.$draStar.length === 1){
        this.draggieStar = new Draggabilly(this.$draStar[0], {containment: '#mapaCristo'});
        this.draggieStarSwitch(false);
    } else {
        console.log("Não há dragStar");
    }
};

// Chave para ligar/desligar o listener do drag
Drag.prototype.draggieStarSwitch = function(opt) {
    if(opt) {
        if( ! this.draggieStar ) this.draggieStarInitialize();
        if( $.browser.mobile ){
            this.draggieStar.on( 'dragEnd', $.proxy(this.onDragEnd, this));
            this.draggieStar.enable();
        } else {
            $(document).on('mousemove', $.proxy(this.moveMouse, this));
            $(document).on('click', $.proxy(this.onDragEnd, this));
        }

        this.$draStar.addClass('movendo');
        this.cristo.Animacao.apagarEstrelas();
    } else {
        if( this.draggieStar ){
            this.draggieStar.off( 'dragEnd', $.proxy(this.onDragEnd, this));
            this.draggieStar.disable();
        }

        $(document).off('mousemove', $.proxy(this, this.moveMouse));
        $(document).off('click', $.proxy(this, this.onDragEnd));

        this.$draStar.removeClass('movendo shake animated');
        this.cristo.Animacao.piscarEstrelas();
    }
};

Drag.prototype.moveMouse = function(ev) {
    var parentOffset = this.$mapaCristo.offset(); 
    // var parentOffset = $('#enviarMensagem').offset(); 
    var relX = ev.pageX - parentOffset.left;
    var relY = ev.pageY - parentOffset.top;// + $(window);
    this.$draStar.css({
        left: relX,
        top: relY
    });
};

// Get / Set
Drag.prototype.draggieStarSetPosition = function(o) { this.draggieStarPosition = o; };
Drag.prototype.draggieStarGetPosition = function() { return this.draggieStarPosition; };

// Verifica se o ponto escolhido é válido
Drag.prototype.validaPosicao = function(posicao) {
    var porcX       = ( posicao.x * 100 ) / this.$mapaCristo.width(),
        porcY       = ( posicao.y * 100 ) / this.$mapaCristo.height(),
        posI        = Math.floor( porcX / 2 ),
        posJ        = Math.floor( porcY / 2 ),
        posProjI    = Math.floor(posI / 5),
        posProjJ    = Math.floor(posJ / 5),
        hit         = this.$mapaCristo.find('> .area.posicao-' + posI + '-' + posJ + ':eq(0)'),
        hitProjecao = $('#cristo .mapa-cristo-projecao .area.posicao-' + posProjI + '-' + posProjJ + ':eq(0)');

    // Debug
    // hit.show();
    // hitProjecao.show();

    return {
        "Pos_X": posProjI,
        "Pos_Y": posProjJ,
        "Porcentagem_X": porcX.toFixed(2).replace(".",","),
        "Porcentagem_Y": porcY.toFixed(2).replace(".",","),
        "hit": hit
    }
}

Drag.prototype.shake = function() {
    var drag = this;
    this.$draStar.addClass('animated shake');
    TweenMax.delayedCall(0.6, function() {
        drag.$draStar.removeClass('animated shake');
    });
};

// Method do evento quando para de arrastar
Drag.prototype.onDragEnd = function( instance, event, pointer ) {
    var difer = $("#cristo").offset();

    if( instance instanceof Draggabilly ){
        var posicao = instance.position;
        var o = this.validaPosicao({x: posicao.x - difer.left, y: posicao.y - difer.top });
    } else {
        var parentOffset = this.$mapaCristo.offset();
        var dposicao = this.$draStar.position();
        // var posicao = {x: dposicao.left - parentOffset.left, y: dposicao.top - parentOffset.top };
        var posicao = {x: dposicao.left, y: dposicao.top };
        var o = this.validaPosicao(posicao);
    }

    if(!!o.hit) {
        if (o.hit.length === 1) {
            if(o.hit.hasClass('area-ok')) {
                o.hit = null;
                this.draggieStarSetPosition(o);
                this.draggieStarSwitch(false);
                window.location.hash = "#/enviar/2";
            } else {
                this.shake();
            }
        } else {
            this.shake();
        }
    } else {
        this.shake();
    }
}
//
