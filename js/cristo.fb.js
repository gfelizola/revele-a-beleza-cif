// Facebook
// Botão de login
var $fbAuth = $('#fbAuth');

window.fbAsyncInit = function() {
    FB.init({
        appId: '397675507030249',
        status: true,
        cookie: true,
        xfbml: true
    });

    // Ativa o clique do botão para efetuar o login
    $fbAuth.on('click.fbLogin', FBLogin);
};

// Load the SDK asynchronously
(function(d) {
    var js, id = 'facebook-jssdk',
        ref = d.getElementsByTagName('script')[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement('script');
    js.id = id;
    js.async = true;
    js.src = "//connect.facebook.net/pt_BR/all.js";
    ref.parentNode.insertBefore(js, ref);
}(document));

// Method do evento de clique do Botão de login
function FBLogin(ev) {
    ev = ev || event;
    ev.preventDefault();
    cristo.Animacao.animarEnvioEtapa2();
    FB.login(function(response) {
        if (response.authResponse) {
            cristo.Usuario.loginSiteSwitch(false);
            $fbAuth.off('click.fbLogin', FBLogin);
            dadosUserFacebookAPI();
        } else {
            console.log('O usuário cancelou o login ou não autorizou.');
        }
    }, {scope: 'email'});
}

// Callback de login ok
function dadosUserFacebookAPI() {
    FB.api('/me', function(response) {
        var dados = {
            "UsuarioFB": response.id,
            "Nome": response.name,
            "Email": response.email,
            "TipoUsuario": 2 // tipos - 1: Site | 2: Facebook | 3: Twitter
        }
        cristo.Usuario.setDados(dados);
        window.location.hash = "#/enviar/3";
    });
}