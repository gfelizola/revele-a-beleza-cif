var  Navegacao = function(cInstance){
    this.cristo = cInstance;
    this.estrelaAtual = null;
    this.$enviarMensagem = $('#enviarMensagem');
    this.$etapas = this.$enviarMensagem.find('.etapa');
}

Navegacao.prototype.init = function() {
    var nav = this;
    Satnav({
        html5: false,
        force: false,
        poll: 100
    })
    .navigate({
        path: 'enviar/{etapa}/?{interna}',
        directions: function(params) {
            window.ga('send', 'pageview', '/enviar-mensagem-etapa-' + params.etapa);
            nav['etapa' + params.etapa](params.interna);
        }
    })
    .navigate({
        path: 'visualizar/{ID}/?',
        directions: function(params) {
            window.ga('send', 'pageview', '/visualizar-mensagem/' + (params.ID || ''));
            nav.mostrarEstrelas(params.ID);
        }
    })
    .navigate({
        path: 'Visualizar/{ID}/?',
        directions: function(params) {
            window.ga('send', 'pageview', '/visualizar-mensagem/' + (params.ID || ''));
            nav.mostrarEstrelas(params.ID);
        }
    })
    .navigate({
        path: 'sobre-cristo/?',
        directions: function(params) {
            $.fancybox('#sobreCristo',{
                width     : 1000,
                height    : 620,
                padding   : 0,
                fitToView : false,
                autoSize  : false,
                closeClick: false,
                afterClose: function() {
                    window.location.hash = 'inicio';
                }
            });
        }
    })
    .navigate({
        path: 'campanha?',
        directions: function(params) {

            $.fancybox({
                href      : 'http://www.youtube.com/embed/' + window.dicionario[nav.cristo.idioma].videoCampanha + '?autoplay=1',
                type      :'iframe',
                maxWidth  : 800,
                maxHeight : 600,
                fitToView : false,
                width     : '70%',
                height    : '70%',
                autoSize  : false,
                closeClick: false,
                afterClose: function() {
                    window.location.hash = 'inicio';
                }
            });
        }
    })
    .navigate({
        path: 'inicio/?',
        directions: function(params) {
            window.ga('send', 'pageview', '/inicio/');
        }
    })
    .otherwise({
        path: '/inicio'
    })
    .change(function(params,old) {
        if( ! params.etapa ){ //não está enviando
            nav.cristo.Animacao.fecharEtapaAtual(function(){
                nav.cristo.Animacao.etapaAtual = 0;
            });

            TweenMax.to(nav.cristo.Drag.$draStar, 0.5, {autoAlpha: 0});
            nav.cristo.Drag.draggieStarSwitch(false);

            nav.cristo.Animacao.animarCristo();
            $('#enviarMensagem').addClass('hidden');
            TweenMax.to($('#conteudo .bt-enviar-mensagem'), 0.5, {autoAlpha: 1});

            nav.cristo.Estrelas.iniciarFila();

        } else {  //enviando mensagem
            nav.cristo.Animacao.pararAnimacaoDoCristo();
            TweenMax.to($('#conteudo .bt-enviar-mensagem'), 0.5, {autoAlpha: 0});

            if( nav.cristo.Estrelas.chamadaFila != null ){
                TweenMax.killDelayedCallsTo( nav.cristo.Estrelas.carregarNovaFila );
            }
        }
    })
    .go();

    if( window.location.href.toLowerCase().indexOf('visualizar') < 0 ){
        this.mostrarEstrelas();
    }
};


/*==============================
 * Estrelas
/*==============================*/
Navegacao.prototype.mostrarEstrelas = function(codigo) {
    console.log('Navegacao.mostrarEstrelas');

    // var retorno = dadosEstrelas;
    this.estrelaAtual = codigo || this.estrelaAtual;
    this.cristo.Servico.ListarEstrelas(function(retorno) {
        this.cristo.Animacao.animarQtdeMensagens(retorno.Total + 12000);
        this.cristo.Estrelas.popular(retorno.Mensagens, codigo, true);
    }, this.estrelaAtual);
}

/*==============================
 * Etapas para envio de mensagem
/*==============================*/

// Etapa 1
Navegacao.prototype.etapa1 = function() {
    if(this.cristo.Animacao.etapaAtual != 1) {
        this.cristo.Animacao.animarEnvioEtapa1();

        // Estrela
        TweenMax.set(this.cristo.Drag.$draStar[0], {opacity: 0});
        this.cristo.Drag.$draStar.removeClass('hidden');
        TweenMax.to(this.cristo.Drag.$draStar[0], .5, {delay: 1, autoAlpha: 1});

        // Ativa o drag da Estrela
        this.cristo.Drag.draggieStarSwitch(true);
    }
};

// Etapa 2
Navegacao.prototype.etapa2 = function(interna) {
    if(this.cristo.Drag.draggieStarPosition === null) {
        window.location.hash = "#/inicio";
    } else {
        this.cristo.Animacao.animarEnvioEtapa2();
    }
};

// Etapa 3
Navegacao.prototype.etapa3 = function(interna) {
    if(this.cristo.Usuario.dados === null) {
        window.location.hash = "#/inicio";
    } else {
        this.cristo.Mensagem.enviarMsgSwitch(true);
        this.cristo.Animacao.animarEnvioEtapa3();
        this.cristo.Servico.CarregarCaptcha();
        //showRecaptcha('recaptchaShow');
    }
};

// Etapa 4
Navegacao.prototype.etapa4 = function(interna) {
    if(this.cristo.Mensagem.mensagemEnviada === false) {
        window.location.hash = "#/inicio";
    } else {
        this.cristo.Mensagem.mensagemEnviada = false;
        this.cristo.Animacao.animarEnvioEtapa4();

        // conversão
        window.cifConverteu();
    }
};
//
