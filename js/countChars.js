// Count Chars
/*
usage:
jQuery('textarea').countChars({
"selectorCounter":" ~ span.counter:eq(0)",
"total": 145
});
*/
;
(function($) {
    var opts;
    var methods = {
        init: function(options) {
            opts = $.extend({
                'total': 145,
                'createCounter': false,
                'selectorCounter': false,
                'counter': null
            }, options);

            return this.each(function() {
                var $this = $(this);
                $this.bind('keyup', methods.count);
                if (opts.selectorCounter && !opts.createCounter)
                    opts.counter = $(this).parent().find(opts.selectorCounter);
            });
        },
        count: function(e) {
            e = e || event;
            var len = e.target.value.length;
            opts.counter.text(opts.total - len);
            if (len >= opts.total) {
                e.target.value = e.target.value.substring(0, opts.total);
                opts.counter.text(0);
            }
        },
        create: function() {

        },
        destroy: function() {
            return this.each(function() {
                var $this = $(this);
                $this.unbind('keyup');
            });
        }
    };

    $.fn.countChars = function(method) {
        if (methods[method])
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        else if (typeof method === 'object' || !method)
            return methods.init.apply(this, arguments);
        else
            $.error('Method ' + method + ' does not exist');
    };
})(jQuery);