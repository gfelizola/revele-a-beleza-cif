var Carregamento = function(cInstance){
    this.cristo = cInstance;
    this.carregador = null;
    this.mapa = {};
    this.arquivos = [
        { src:'img/botao-bg.jpg', id:'conteudo-img-botao-bg.jpg', type:createjs.LoadQueue.IMAGE },
        { src:'img/bt-enviar-mensagem.png', id:'conteudo-img-bt-enviar-mensagem.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/cristo.png', id:'conteudo-img-cristo.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/estrela-cristo-1.png', id:'conteudo-img-estrela-cristo-1.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/estrela-cristo-2.png', id:'conteudo-img-estrela-cristo-2.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/estrela-cristo-3.png', id:'conteudo-img-estrela-cristo-3.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/estrela-cristo-4.png', id:'conteudo-img-estrela-cristo-4.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/estrela-cristo.png', id:'conteudo-img-estrela-cristo.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/estrelas-qtde.png', id:'conteudo-img-estrelas-qtde.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/footer-borda.png', id:'conteudo-img-footer-borda.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/fundo.jpg', id:'conteudo-img-fundo.jpg', type:createjs.LoadQueue.IMAGE },
        { src:'img/icone-video.png', id:'conteudo-img-icone-video.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/logo-amigo-cristo.png', id:'conteudo-img-logo-amigo-cristo.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/logo-cif-fixo.png', id:'conteudo-img-logo-cif-fixo.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/logo-cif-footer.png', id:'conteudo-img-logo-cif-footer.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/logo-cif.png', id:'conteudo-img-logo-cif.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/logo-sesc.png', id:'conteudo-img-logo-sesc.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/passoapasso-icone.png', id:'conteudo-img-passoapasso-icone.png', type:createjs.LoadQueue.IMAGE },
        { src:'img/site-image.jpg', id:'conteudo-img-site-image.jpg', type:createjs.LoadQueue.IMAGE },
        { src:'img/txt-chamada.png', id:'conteudo-img-txt-chamada.png', type:createjs.LoadQueue.IMAGE }
    ];
    // NProgress.configure({speed:20});
}

Carregamento.prototype.init = function() {
    //Preload JS
    var fila = new createjs.LoadQueue(true);

    fila.setUseXHR(false);
    fila.addEventListener("complete", this.completou);
    fila.addEventListener("progress", this.progresso);
    fila.addEventListener("error", this.erro);
    fila.loadManifest(this.arquivos, true);

    this.carregador = fila;
    NProgress.start();
};

Carregamento.prototype.parar = function() {
    if (this.carregador) this.carregador.close();
};

Carregamento.prototype.progresso = function(ev) {
    NProgress.inc();
};

Carregamento.prototype.completou = function(ev) {
    var imgCristo = this.cristo.Carregamento.carregador.getResult('conteudo-img-cristo.png');
    $(imgCristo).addClass('jesus');
    $('#cristo').append(imgCristo);
    $('#main').css('background-image', 'url("img/fundo.jpg")');

    NProgress.set(1);
    NProgress.done();
    NProgress.remove();
    this.cristo.Animacao.animarEntrada();
};

Carregamento.prototype.erro = function(ev) {
    console.log("Erro no carregamento", ev);
};