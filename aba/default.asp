<!DOCTYPE html>
<!--[if lt IE 7]> <html lang="en" class="no-js ie6 oldie"> <![endif]-->
<!--[if IE 7]>    <html lang="en" class="no-js ie7 oldie"> <![endif]-->
<!--[if IE 8]>    <html lang="en" class="no-js ie8 oldie"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
<!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
        <title>Revele a Beleza | CIF</title>
        <meta content="" name="description">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        
        <style>
            body {
                margin: 0;
                padding: 0;
                width: 810px;
                height: 886px;
                background: #ffffff url('imagem.jpg') no-repeat left top;
            }

            a {
                display: block;
                position: absolute;
            }

            .peca-amostra {
                width: 170px;
                height: 35px;
                top: 5px;
                left: 635px;
            }

            .envie {
                width: 300px;
                height: 55px;
                top: 184px;
                left: 261px;
            }
        </style>
    </head>
    <body>
        <a href="https://www.facebook.com/CifLimpadores/app_170293279799249" target="_blank" class="peca-amostra" title="Peça sua amostra aqui"></a>
        <a href="http://www.reveleabeleza.com.br" class="envie" target="_blank" title="Envie sua mensagem e faça o Cristo Redentor brilhar"></a>

        <script>
        window.fbAsyncInit = function() {
          FB.init({ appId: '194836367343955' });
          FB.Canvas.setAutoGrow();
        }
        </script>
        <div id="fb-root"></div>
        <script>
          // Load the SDK asynchronously
          (function(d, s, id){
             var js, fjs = d.getElementsByTagName(s)[0];
             if (d.getElementById(id)) {return;}
             js = d.createElement(s); js.id = id;
             js.src = "//connect.facebook.net/en_US/all.js";
             fjs.parentNode.insertBefore(js, fjs);
           }(document, 'script', 'facebook-jssdk'));
        </script>


        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-42152235-2');
        ga('send', 'pageview');
        </script>
  </body>
</html>
