http_path = "/"
css_dir = "css"
sass_dir = "sass"
images_dir = "img"
javascripts_dir = "js"

output_style = :expanded
relative_assets = true
line_comments = true

# Isso faz com que o RespondJS não funcione corretamente
# sass_options = { :debug_info => true }
