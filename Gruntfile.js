'use strict';

module.exports = function(grunt) {

    require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Server de DEV.
        connect: {
            server: {
                options: {
                    port: 9000,
                    livereload: true,
                    open: true
                }
            }
        },

        // Concat
        concat: {
            options: {
                separator: ';',
                footer:'//------------ FuckingWindowsChars -> '
            },
            frameworks: {
                src: [
                    // jQuery
                    'bower_components/jquery/jquery.js',
                    // TweenMax
                    'bower_components/greensock-js/src/uncompressed/TweenMax.js',
                    // PreloadJS
                    'bower_components/PreloadJS/lib/preloadjs-0.4.0.min.js',
                    // Satnav
                    'bower_components/satnav/dist/satnav.js',
                    // NProgress
                    'bower_components/nprogress/nprogress.js',
                    // Fancybox
                    'bower_components/fancybox/lib/jquery.mousewheel-3.0.6.pack.js',
                    'bower_components/fancybox/source/jquery.fancybox.pack.js',
                    // Dependencias do Draggabilly,
                    'bower_components/classie/classie.js',
                    'bower_components/eventEmitter/EventEmitter.js',
                    'bower_components/eventie/eventie.js',
                    'bower_components/get-style-property/get-style-property.js',
                    'bower_components/get-size/get-size.js',
                    // Draggabilly
                    'bower_components/draggabilly/draggabilly.js'
                ],
                dest: 'js/dist/frameworks.js'
            },
            vendor: {
                src: ['js/countChars.js'],
                dest: 'js/dist/vendor.js'
            },
            app: {
                src: [
                    'js/helpers.js',
                    'js/cristo.dicionario.js',
                    'js/cristo.loading.js',
                    'js/cristo.navegacao.js',
                    'js/cristo.drag.js',
                    'js/cristo.usuario.js',
                    'js/cristo.mensagem.js',
                    'js/cristo.servico.js',
                    'js/cristo.estrelas.js',
                    'js/cristo.animacao.js',
                    'js/cristo.fb.js',
                    'js/cristo.track.js',
                    'js/cristo.js'
                ],
                dest: 'js/dist/base.js'
            },
            dev: {
                src: [
                    'js/dist/frameworks.js',
                    'js/dist/vendor.js',
                    'js/dist/base.js'
                ],
                dest: 'js/script.js'
            }
        },

        // Validação de arquivos
        jshint: {
            options: {
                jshintrc: ".jshintrc"
            },
            all: ["Gruntfile.js"]
        },

        // Uglify
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
                properties: true,
                compress: {
                    global_defs: {
                        "DEBUG": false
                    },
                    dead_code: true
                }
            },
            target: {
                files: {
                    'js/script.js': ['js/script.js']
                }
            }
        },
        // Compass
        compass: {
            dev: {
                options: {
                    config: 'config.dev.rb'
                }
            },
            prod: {
                options: {
                    config: 'config.rb'
                }
            }
        },

        // Watch
        watch: {
            options: {
                livereload: true
            },
            sass: { // Compilação
                files: 'sass/**',
                tasks: 'compass',
                options: {
                    livereload: false
                }
            },
            css: {
                files: ['css/*.css']
            },
            js: { // Concatenação
                files: ['js/*.js', 'Gruntfile.js', '!js/script.js', '!js/dist/*.js'],
                tasks: ['jshint', 'concat']
            },
            others: {
                files: ['./**/*.html', './img/**/*'],
                options: {
                    livereload: true
                }
            }
        },

        copy: {
            deploy: {
                files: [
                    // css
                    {src: ['css/*'], dest: '.deploy/', filter:'isFile'},
                    {src: ['css/fonts/*'], dest: '.deploy/'},
                    // js
                    {src: ['js/script.js'], dest: '.deploy/js/script.js'},
                    {src: ['js/cristo.conversion.js'], dest: '.deploy/js/cristo.conversion.js'},
                    {src: ['js/modernizr.min.js'], dest: '.deploy/js/modernizr.min.js'},
                    {src: ['js/html5.respond.min.js'], dest: '.deploy/js/html5.respond.min.js'},
                    // img
                    {src: ['img/*'], dest: '.deploy/', filter: 'isFile'},
                    {src: ['img/fancybox/*'], dest: '.deploy/', filter: 'isFile'},
                    // outros
                    {src: ['*.html'], dest: '.deploy/', filter: 'isFile'}
                    // ,{src: ['*.xml'], dest: '.deploy/', filter: 'isFile'},
                    // {src: ['*.txt'], dest: '.deploy/', filter: 'isFile'}
                ]
            },
            fenix: {
                files: [
                    {expand: true, cwd: ".deploy/", src: "**", dest: "/Volumes/Web/Unilever/Cif/"}
                ]
            }
        },

        rsync: {
            options: {
                args: ["-ravzup"],
                exclude: [".git*","*.scss",".sass-cache","node_modules","bower_components"],
                syncDestIgnoreExcl: true
            },
            fenix: {
                options: {
                    src: "./.deploy/",
                    dest: "/Volumes/Web/Unilever/Cif/"
                }
            },
            stage: {
                options: {
                    src: "./.deploy/",
                    dest: "/var/www/site",
                    host: "user@staging-host"
                }
            },
            prod: {
                options: {
                    src: "./.deploy/",
                    dest: "/var/www/site",
                    host: "user@live-host"
                }
            }
        }
    });

    grunt.registerTask('default',   ['concat',  'compass:dev'                   ]);
    grunt.registerTask('server',    ['default', 'connect',      'watch'         ]);
    grunt.registerTask('build',     ['concat',  'uglify',       'compass:prod'  ]);

    //deploys
    grunt.registerTask('df',        ['build',   'copy']);
};